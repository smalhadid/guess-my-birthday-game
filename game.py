from random import randint
name = input("Hi! What is your name?")

day_low = 1
day_high = 30
month_low = 1
month_high = 12
year_low = 1924
year_high = 2004


for num in range(5):

    guess = randint(year_low, year_high)

    print(name, "were you born in ", randint(day_low, day_high), " / ", randint(month_low, month_high), " / ", guess)
    reply = input("yes, later or earlier?")
    print(reply)

    if reply == "yes" :
            print("I knew it!")
            break
    elif reply == "later":
            print("Drat! Lemme try again!")
            year_low = guess + 1
    elif reply == "earlier":
            print("Drat! Lemme try again!")
            year_high = guess - 1
    else:
            print("I have other things to do. Good bye")
